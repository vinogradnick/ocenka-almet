import Banner from "../components/Banner";
import BannerContent from "../components/BannerContent";
import BaseTemplate from "../components/BaseTemplate";
import Nav from "../components/Nav";
import OcenkaType from "../components/OcenkaType";
export default function Home() {
  return (
    <div>
      <BaseTemplate>
        <Banner>
          <Nav />
          <BannerContent
            title={"Оценка альметьевск"}
            subtitle="Оценка имущества, недвижимости (земельных участков, квартир,
                жилых домов, коммерческой недвижимости), оборудования, ущерба от
                затопления и пожара, бизнеса"
          />
        </Banner>
        <OcenkaType />
      </BaseTemplate>
    </div>
  );
}
