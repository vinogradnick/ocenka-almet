const withImages = require("next-images");
const sitemap = require("nextjs-sitemap-generator");
const withSass = require("@zeit/next-sass");
