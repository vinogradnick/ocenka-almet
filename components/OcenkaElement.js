import React from "react";
import Link from "next/link";

function OcenkaElement({ title, image, idx }) {
  return (
    <Link href={"price/" + idx}>
      <a className="column is-4 is-horizontal-center">
        <div className="card card-dark card-1">
          <div className="card-image">
            <figure className="image is-4by3">
              <img src={image} alt={title} />
            </figure>
          </div>
          <div className="card-content">
            <div className="content has-text-centered text-light subtitle">
              {title}
              <br />
            </div>
          </div>
        </div>
      </a>
    </Link>
  );
}

export default OcenkaElement;
