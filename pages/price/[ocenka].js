import React from "react";
import BaseTemplate from "../../components/BaseTemplate";
import Banner from "../../components/Banner";
import BannerContent from "../../components/BannerContent";
import Nav from "../../components/Nav";
import jsonData from "../../data.json";
import { useRouter } from "next/router";
import Table from "../../components/Table";
import _ from "lodash";

function Price(props) {
  const router = useRouter();
  const { ocenka } = router.query;
  const [data, setData] = React.useState([]);
  React.useEffect(() => {
    setData(jsonData.collection[ocenka]);
  }, [ocenka]);
  const children = _.get(data, "children");
  console.log(children);
  return (
    <BaseTemplate>
      <React.Fragment>
        <Banner image={data && data.image}>
          <Nav />
          <BannerContent title={(data && data.title) || "Прайс-Лист"} />
        </Banner>
        <div className="container mt-6 mb-6 ">
          {children && <Table data={children} />}
        </div>
      </React.Fragment>
    </BaseTemplate>
  );
}

export default Price;
