import React from "react";

function Footer() {
  const contacts = [
    {
      key: "Адрес",
      value: "Альметьевск (423454), с. Тихоновка, ул. Луговая,54",
    },
    { key: "Телефон", value: "+7 (927) 471-80-01" },
    {
      key: " Моб. телефон",
      value: "+7 (987) 008-92-18",
    },
    {
      key: "Электронная почта",
      value: "79274718001@yandex.ru",
    },
    {
      key: "Время работы",
      value: "пн.-пт. 08:00 — 20:00 (без перерыва)",
    },
  ];
  return (
    <footer className="footer">
      <div className="content has-text-centered">
        <section className="section">
          <span className="title mb-1">Контактная информация</span>
        </section>

        <div className="columns" style={{ flexDirection: "column" }}>
          {contacts.map((contact) => (
            <React.Fragment>
              <div className="column is-12 has-text-centered">
                <span className="has-text-weight-bold	">
                  {contact.key}:&nbsp;&nbsp;
                </span>
                {contact.value}
              </div>
            </React.Fragment>
          ))}
        </div>
      </div>
    </footer>
  );
}

export default Footer;
