import React from "react";
import uuidv4 from "./../utils/uuid";
import _ from "lodash";
function Selector({ data, onChange, selected }) {
  return (
    <div className="field ">
      <label className="label">Тип оценки</label>
      <div className="control">
        <div className="select is-fullwidth">
          <select name="types" onChange={(e) => onChange(data[e.target.value])}>
            {data &&
              data.map((item, idx) => (
                <option
                  selected={selected && selected.title == item.title}
                  key={uuidv4()}
                  value={idx}
                >
                  {item.title}
                </option>
              ))}
          </select>
        </div>
      </div>
    </div>
  );
}
function CheckBox({ label, onChange, value }) {
  return (
    <React.Fragment>
      <label className="checkbox">
        <input
          type="checkbox"
          checked={value}
          onChange={(e) => onChange(!value)}
        />
        &nbsp;{label}
      </label>
    </React.Fragment>
  );
}

function LocalItem({ item, onSelect, selectedItems, parent }) {
  const isSelected = `${_.get(item, "children") && "menu-label"} ${
    _.find(selectedItems, item) && "is-active"
  }`;
  return (
    <React.Fragment>
      <p
        onClick={() =>
          !_.get(item, "children") && onSelect({ ...item, parent })
        }
        className={isSelected}
      >
        {item.title}
      </p>
      <ul className="menu-list">
        {item &&
          item.children &&
          item.children.map((ocenka) => (
            <li>
              <LocalItem
                onSelect={onSelect}
                item={ocenka}
                parent={item.title}
              />
            </li>
          ))}
      </ul>
    </React.Fragment>
  );
}
function divideNumberByPieces(x, delimiter) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter || " ");
}

function Counter({ count, onChange }) {
  return (
    <>
      <a
        className="is-size-5	 has-text-weight-bold"
        onClick={() => count > 0 && onChange(count + 1)}
      >
        +
      </a>
      &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
      <a
        className="is-size-5 has-text-weight-bold"
        onClick={() => count > 0 && onChange(count - 1)}
      >
        -
      </a>
    </>
  );
}

function Calculator({ data }) {
  const [ocenkaType, setOcenkaType] = React.useState(undefined);
  const [status,setStatus]=React.useState(false);
  const [cost, setCost] = React.useState(0);
  const [selectedItems, setSelectedItems] = React.useState({});
  const addItem = (item) => {
    item.count = 1;
    item.uuid = uuidv4();
    const value = Object.values(selectedItems).find(
      (s) => s.title === item.title
    );
    if (!value) {
      selectedItems[item.uuid] = item;
    }
    setSelectedItems({ ...selectedItems });
  };
  const addCount = (item) => {
    if(item.count==0){
      delete selectedItems[item.uuid];
    }
    else{
      selectedItems[item.uuid] = item;

    }
    setSelectedItems({ ...selectedItems });

  };
  console.log(selectedItems);
  React.useEffect(() => {
    setCost(_.sumBy(Object.values(selectedItems), (x) => x.price * x.count  ));
  });
  return (
    <div>
      <div className="card">
        <div className="card-content">
          <p className="subtitle has-text-centered">Калькулятор расчетов </p>
          <CheckBox label="Для суда" value={status} onChange={setStatus} />
          
         
         {status ?  <p className="title">Расчетная сумма для суда : {cost.toFixed(2)*2} Ꝑ</p>:
         <p className="title">Расчетная сумма: {cost.toFixed(2)} Ꝑ</p>} 
          <div className="panel">
            {selectedItems &&
              Object.values(selectedItems).map((item, idx) => (
                <a className="panel-block is-active" key={item.uuid}>
                  <div className="columns " style={{ width: "100%" }}>
                    <div className="column is-1">
                      <span className="has-text-weight-bold">{idx + 1}</span>
                    </div>
                    <div className="column is-6">
                      <span className="has-text-weight-light">
                        {item.parent}&nbsp;&nbsp;
                      </span>
                      <span>{item.title}</span>
                    </div>
                    <div className="column is-2">
                      <span className="has-text-weight-bold">
                        {item.price.toFixed(2)} Ꝑ
                      </span>
                    </div>
                    <div className="column is-2">
                      <span className="has-text-weight-bold">{item.count}</span>
                    </div>
                    <div className="column is-2">
                      <Counter
                        count={item.count}
                        onChange={(value) =>
                          addCount({ ...item, count: value })
                        }
                      />
                    </div>
                  </div>
                </a>
              ))}
          </div>

          <Selector
            selected={ocenkaType}
            data={data}
            onChange={setOcenkaType}
          />
          <aside className="menu">
            {ocenkaType && (
              <LocalItem
                parent=""
                selectedItems={selectedItems}
                item={ocenkaType}
                onSelect={addItem}
              />
            )}
          </aside>
        </div>
        <footer className="card-footer">
          <p className="card-footer-item">Расчитать</p>
          <p className="card-footer-item">Отмена</p>
        </footer>
      </div>
    </div>
  );
}

export default Calculator;
