import React from "react";
import _ from "lodash";
function TableChild({ dataItem }) {
  const priceRange = `${_.get(dataItem, "price.start") || ""}-${
    _.get(dataItem, "price.end") || ""
  }`;
  const price = _.get(dataItem, "price");

  return (
    <React.Fragment>
      <tr>
        <td width="36">&nbsp;</td>
        <td colSpan={dataItem.children && 2}>
          <p className={`${dataItem.children && "has-text-weight-bold"}`}>
            {dataItem.title}
          </p>
        </td>
        {!dataItem.children && (
          <td width="120">
            <strong>
            {dataItem.prefix} {priceRange.length > 2 ? priceRange : price.toFixed(2)} Ꝑ
            </strong>
          </td>
        )}
      </tr>

      {dataItem.children &&
        dataItem.children.map((item) => <TableChild dataItem={item} />)}
    </React.Fragment>
  );
}

function Table({ data }) {
  console.log(data);
  return (
    <table
      className="table is-bordered is-striped is-narrow is-hoverable is-fullwidth
    "
    >
      <thead>
        <tr>
          <th width="36">№</th>
          <th width="432">Наименование</th>
          <th width="120">Тариф, рублей</th>
        </tr>
      </thead>
      <tbody>
        {data && data.map((item) => <TableChild dataItem={item} />)}
      </tbody>
    </table>
  );
}

export default Table;
