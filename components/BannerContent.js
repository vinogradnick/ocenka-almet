import React from "react";

function BannerContent({ title, subtitle }) {
  return (
    <div className="container">
      <section className="hero is-medium">
        <div className="hero-body">
          <div className="container has-text-centered">
            <h1 className="title is-1  ">{title}</h1>
            <div className="container">
              <h2 className="subtitle ">{subtitle}</h2>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default BannerContent;
/*
Оценка имущества, недвижимости (земельных участков, квартир,
                жилых домов, коммерческой недвижимости), оборудования, ущерба от
                затопления и пожара, бизнеса
                */
