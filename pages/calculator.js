import React from "react";
import Banner from "../components/Banner";
import BannerContent from "../components/BannerContent";
import BaseTemplate from "../components/BaseTemplate";
import Nav from "../components/Nav";
import Calculator from "../components/Calculator";
import jsonData from "../data.json";

function CalculatorPage() {
  return (
    <BaseTemplate>
      <React.Fragment>
        <Banner>
          <Nav />
          <BannerContent title="Калькулятор" />
        </Banner>
        <div className="container mt-6 mb-6 ">
          <Calculator data={jsonData.collection} />
        </div>
      </React.Fragment>
    </BaseTemplate>
  );
}

export default CalculatorPage;
