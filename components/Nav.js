import { route } from "next/dist/next-server/server/router";
import React from "react";
import Link from "next/link";

function Nav() {
  const routes = [
    { path: "/", name: "Главная" },
    { path: "/calculator", name: "Калькулятор" },
  ];
  const [burgerActive, setBurgerActive] = React.useState(false);
  return (
    <div>
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            Оценка Альметьевск &nbsp;
          </a>
          <a
            role="button"
            className="navbar-burger burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
            onClick={() => setBurgerActive(!burgerActive)}
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div className={`navbar-menu ${burgerActive ? "is-active" : ""}`}>
          <div className="navbar-end">
            {routes.map((route) => (
              <Link href={route.path}>
                <a className="navbar-item">{route.name}</a>
              </Link>
            ))}

            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-dark card-2">Консультация</a>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Nav;
