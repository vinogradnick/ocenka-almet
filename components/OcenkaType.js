import React from "react";
import OcenkaElement from "../components/OcenkaElement";
import dataJson from "../data.json";
function OcenkaType() {
  const types = dataJson.collection;
  return (
    <section className="section">
      <div className="container">
        <h1 className="title has-text-centered mb-6">Типы оценки</h1>
        <div className="columns is-multiline">
          {types.map((item, idx) => (
            <OcenkaElement image={item.image} title={item.title} idx={idx} />
          ))}
        </div>
      </div>
    </section>
  );
}

export default OcenkaType;
