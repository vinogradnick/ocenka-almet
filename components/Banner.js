import React from "react";

function Banner({ children, image }) {
  return (
    <div
      className="banner"
      style={{
        backgroundSize: "cover",
      }}
    >
      {children}
    </div>
  );
}

export default Banner;
