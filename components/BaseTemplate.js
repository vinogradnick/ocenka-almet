import React from "react";
import Head from "next/head";
import Footer from "../components/Footer";
import { YMInitializer } from "react-yandex-metrika";

function BaseTemplate({ children }) {
  return (
    <div>
      <Head>
        <title>Оценка Альметьевск</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet"
        />
        <meta name="yandex-verification" content="0b4388bb84955187" />
        <meta
          name="description"
          content="Оценка недвижимости, Оценка Альметьевск,оценка альметьевск, оценка недвижимости"
        />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css"
        />
      </Head>
      {children}
      <Footer />
      <YMInitializer accounts={[67692214]} version="2" />
    </div>
  );
}

export default BaseTemplate;
